 //initialize starting point for counting articles
 let lowerLimit = 0;
 let upperLimit = 30;

async function showArticles(lowerLimit, upperLimit) {
  
  const app = document.getElementById('root');
  const container = document.createElement('div');
  container.setAttribute('class', 'container1');
  app.appendChild(container);

  // read our JSON
  let response = await fetch('https://hacker-news.firebaseio.com/v0/newstories.json');
  let articleIds = await response.json();

  // wait 3 seconds
  await new Promise((resolve, reject) => setTimeout(resolve, 3000));

  articleIds.slice(lowerLimit,upperLimit).forEach(async (id)=>{
    let articleResponse = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
    let article = await articleResponse.json()
    // getMetaData(article.url)

    if(article.url){
      console.log(article.url)
      createArticleCard (article, container);
    }
    //increament counter for start and end limit each time the function is called
    lowerLimit +=30;
    upperLimit +=30;

  })
}

function createArticleCard (article, container){

  const card = document.createElement('div');
  card.setAttribute('class', 'card');

  const h1 = document.createElement('h1');
  h1.textContent = article.title;

  const p = document.createElement('p');
  p.textContent = `${article.url}`;

  container.appendChild(card);
  card.appendChild(h1);
  card.appendChild(p);

}

// async function getMetaData(url){
//   // fetchJsonp(url)
//   //   .then(res => res.json())
//   //   .then(json => console.log(`from meta:${json}`));
//   // wait 3 seconds
//   await new Promise((resolve, reject) => setTimeout(resolve, 5000));
//   let response = await fetch(url);
//   let responseJson = await response.json();
//   console.log(`from meta:${responseJson}`)
// }

showArticles(lowerLimit, upperLimit);

