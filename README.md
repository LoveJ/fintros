# Fintros App

This application is a basic attempt to clone a wealthsimp,e page.

The design choice is because there is no heavy lifting required 
and most content is just pulled off from an external public API which
requires no form of authentication.

In addition, since I've used the basic building blocks for any good web
application, this can easily be translated into any framework or tech stack
(ruby on rails, react, node backend, etc) should some heavy lifting be required
in future.


